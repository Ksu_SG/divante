// *** SLIDEOUT MOBILE MENU ***
  var mobile_nav = $('#mobile-nav');
  var mobile_nav_width = $('#mobile-nav').width();

  function show_nav () {
    mobile_nav.animate({
      left: "0px"
    });
  }
  function hide_nav () {
    mobile_nav.animate({
      left: -mobile_nav_width
    }, 250);
  }

  $('#trigger').on('click', function (event) {
    mobile_nav.toggleClass("open");

    if (mobile_nav.hasClass("open")) {
      show_nav ();
    } else {
      hide_nav ();
    }
    event.stopPropagation();
  });

  $(document).click(function () {
    hide_nav ();
    $(mobile_nav).removeClass("open");
  });
